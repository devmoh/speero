<?php

namespace Database\Factories;

use App\Models\Invoiceitem;
use Illuminate\Database\Eloquent\Factories\Factory;

class InvoiceitemFactory extends Factory
{
    /**
     * The name of the factory's corresponding model.
     *
     * @var string
     */
    protected $model = Invoiceitem::class;

    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        return [
            'invoice_id'   => \App\Models\Invoice::factory(1)->create()->first(),
            'product_name' => $this->faker->text(30),
        ];
    }
}
