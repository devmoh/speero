<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Factories\HasFactory;

class Invoiceitem extends Model
{
    use HasFactory;

    /**
     * Get the items that owns the invoice.
     */
    public function invoice()
    {
        return $this->hasOne(Invoice::class,'id','invoice_id')->select('id', 'invoice_number','customer_id');
    }

    
}
