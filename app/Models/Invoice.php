<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Factories\HasFactory;

class Invoice extends Model
{
    use HasFactory;

    /**
     * Get the items that owns the invoice.
     */
    public function customer()
    {
        return $this->hasOne(Customer::class, 'id', 'customer_id')->select('id', 'name');

    }

}
