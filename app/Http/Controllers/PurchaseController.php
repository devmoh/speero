<?php

namespace App\Http\Controllers;

use App\Models\Invoiceitem;

class PurchaseController extends Controller
{
    // export csv function with chuncks
    public function exportexportWithChuncks()
    {

        // cloumns name of file
        $Headerscolumns = [
            'Customer Name',
            'Invoice  Number',
            'Product  Name',
        ];
        return response()->streamDownload(function () use ($Headerscolumns) {
            $file = fopen('php://output', 'w+');
            fputcsv($file, $Headerscolumns);
            Invoiceitem::with('invoice.customer')->chunk(50, function ($items) use ($file) {
                foreach ($items as $item) {
                    $data = [$item['invoice']['customer']['name'], $item['invoice']['invoice_number'], $item['product_name']];
                    fputcsv($file, $data);
                }
            });

            fclose($file);
        }, 'PurchaseChuncks' . date('d-m-Y') . '.csv');

    }

    // export csv function with generator
    public function exportWithGenerator()
    {
        // cloumns name of file
        $Headerscolumns = [
            'Customer Name',
            'Invoice  Number',
            'Product  Name',
        ];
        return response()->streamDownload(function () use ($Headerscolumns) {

            $file = fopen('php://output', 'w+');
            fputcsv($file, $Headerscolumns);

            $items = $this->purchaseGenerator();
            foreach ($items as $item) {
                $data = [$item['invoice']['customer']['name'], $item['invoice']['invoice_number'], $item['product_name']];
                fputcsv($file, $data);
            }

            fclose($file);
        }, 'PurchaseGenerator' . date('d-m-Y') . '.csv');

    }

    public function purchaseGenerator()
    {

        foreach (Invoiceitem::with('invoice.customer')->cursor() as $user) {
            yield $user;
        }
    }

}
